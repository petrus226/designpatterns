# -*- coding: utf-8 -*-
import unittest
from pizza import *

class PizzaDreamTest(unittest.TestCase):

    def test_has_dough(self):
        pizzaFactory = PizzaFactory()
        pizza = pizzaFactory.return_flavor()

        result = pizza.ingredients()
        
        self.assertEqual(result, 'dough')

    def test_margarita(self):
        pizzaFactory = PizzaFactory()
        pizza = pizzaFactory.return_flavor('Margarita')
        expected_ingredients = 'dough, mozzarella, tomato, basilico'

        result = pizza.ingredients()

        self.assertEqual(result, expected_ingredients)

    def test_peperoni(self):
        pizzaFactory = PizzaFactory()
        pizza = pizzaFactory.return_flavor('Peperoni')
        expected_ingredients = 'dough, mozzarella, tomato, peperoni'

        result = pizza.ingredients()

        self.assertEqual(result, expected_ingredients)
        
    def test_has_owner(self):
        pizzaFactory = PizzaFactory()
        pizza = pizzaFactory.return_flavor('Peperoni')
        expected_ingredients = 'dough, mozzarella, tomato, peperoni'

        result = pizza.ingredients()

        self.assertEqual(result, expected_ingredients)
        
if __name__ == '__main__':
    unittest.main()

    