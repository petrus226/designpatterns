class Pizza:
    def __init__(self):
        self._ingredients = 'dough'

    def ingredients(self):
        return self._ingredients

class Margarita:
    def __init__(self):
        self._ingredients = 'dough, mozzarella, tomato, basilico'

    def ingredients(self):
        return self._ingredients

class  Peperoni:
    def __init__(self):
        self._ingredients = 'dough, mozzarella, tomato, peperoni'

    def ingredients(self):
        return self._ingredients

class PizzaFactory:
    pizza = {
            'Margarita': Margarita,
            'Peperoni': Peperoni
    }
    def return_flavor(self, flavor=""):
        if flavor:
            return self.pizza[flavor]()
        return Pizza()
