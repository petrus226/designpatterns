from mamba import *
from expects import *
from coffee import *
from condiments import *

with description('CoffeeMachine') as self:
  with it('returns a number when you ask for a coffee'):
    coffee = Coffee()
    
    result = coffee.cost()

    expect(type(result)).to(equal(int))

  with it('returns coffee description when you ask for a coffee'):
    coffee = Coffee()
    
    result = coffee.description
    
    expect(result).to(equal('coffee'))

  with it('espresso costs 0.89'):
    espresso = Espresso()

    result = espresso.cost()
    
    expect(result).to(equal(0.89))

  with it('espresso return his description'):
    espresso = Espresso()

    result = espresso.get_description()
    
    expect(result).to(equal('espresso coffee'))

  with it('Kopi Luwak return his description'):
    kopi_luwak = KopiLuwak()

    result = kopi_luwak.get_description()
    
    expect(result).to(equal('Kopi Luwak coffee'))

  with it('espresso return his description'):
    espresso = Espresso()
    milky_espresso = Milk(espresso)

    result = milky_espresso.get_description()
    
    expect(result).to(equal('espresso coffee with milk'))


  with it('espresso return his description'):
    espresso = Espresso()
    milky_espresso = Milk(espresso)

    result = milky_espresso.cost()
    
    expect(result).to(equal(1.09))