from coffee import *

class Milk(Coffee):
  condiment = 'milk'
  
  def __init__(self, coffee):
    self.coffee = coffee
    
  def get_description(self):
    return  self.coffee.get_description() + ' with ' + self.condiment

  def cost(self):
    return self.coffee.cost() + 0.2