class Coffee:
  description = 'coffee'

  def cost(self):
    return 50

class Espresso(Coffee):
  variety = 'espresso'

  def cost(self):
    return 0.89

  def get_description(self):
    return self.variety + ' ' + self.description

class KopiLuwak(Coffee):
  variety = 'Kopi Luwak'
  
  def cost(self):
    return 1.5

  def get_description(self):
    return self.variety + ' ' + self.description