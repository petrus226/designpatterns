from mamba import *
from expects import *
from event import *

with description('Runtastic') as self:
  with it('display a competidor registered for the event'):
    attendees = [('Pepe', 'runner')]

    event = Event(attendees= attendees)
    result = event.participants()

    expect(result).to(equal(['Pepe']))

  with it('display two competidors registered for the event'):
    attendees = [('Pepe', 'runner'), ('Lalo', 'runner')]

    event = Event(attendees= attendees)
    result = event.participants()

    expect(result).to(equal(['Pepe', 'Lalo']))

  with it('display what competion is'):
    competition = 'Marathons'

    event = Event(competition=competition)
    result = event.competition

    expect(result).to(equal('Marathons'))


with description('EventAttendee') as self:
  with it('can be a runner'):
    name = 'Pepe'

    runner = RunnerAttendee(name)

    expect(runner.performance()).to(equal('run'))

  with it('can be a marshal'):
    name = 'Paco'

    marshal = MarshalAttendee(name)

    expect(marshal.performance()).to(equal('Ready, Set, Go'))

  with it('can be a triathlete'):
    name = 'Paco'

    trialthlete = TriathleteAttendee(name)

    expect(trialthlete.performance()).to(equal('Run, swim and cycle'))