class Event:
    competition = ''
    attendees = []

    def __init__(self, competition='', attendees=[]):
        self.competition = competition
        self.attendees = []             #mirar como limpiar la clase para que no arraste los participantes
        self.add_competidor(attendees)

    def add_competidor(self, attendees):
        for attendee in attendees:
            name = attendee[0]
            performance = attendee[1]
            self.attendees.append(EventAttendee(name, performance))

    def participants(self):
        x = []
        for attendee in self.attendees:
            x.append(attendee.name)
        return x


class Run:
    def accion(self):
        return 'run'

class Triathlete:
    def accion(self):
        return 'Run, swim and cycle'

class KickOff:
    def accion(self):
        return 'Ready, Set, Go'

class EventAttendee:
    name = ''

    def __init__(self, name='', performance=''):
        self.name = name
        self._performance = performance

    def performance(self):
        return self._performance.accion()

class RunnerAttendee(EventAttendee):
    def __init__(self, name):
        super(RunnerAttendee, self).__init__(name, performance=Run())

class MarshalAttendee(EventAttendee):
    def __init__(self, name):
        super(MarshalAttendee, self).__init__(name, performance=KickOff())

class TriathleteAttendee(EventAttendee):
    def __init__(self, name):
        super(TriathleteAttendee, self).__init__(name, performance=Triathlete())
